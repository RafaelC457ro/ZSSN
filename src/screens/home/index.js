import React from 'react'
import Title from '../../components/ui/title'
import Subtitle from '../../components/ui/subtitle'
import Columns from '../../components/ui/columns'
import Column from '../../components/ui/column'
import {Link} from 'react-router-dom'
import style from './style.css'

const Home = () => (
  <div>
    <Title>
      Welcome to
      <div className="has-text-success">
        <b>Zombie Survival Social Network</b>
      </div>
    </Title>
    <Columns>
      <Column size="1/2">
        <p>
          This was designed for share resources between non-infected humans. You
          can
          <span className={style.needspace}>
            <Link to="/survivor/new">Create a account here</Link>
          </span>
          or find yourself in
          <span className={style.needspace}>
            <Link to="/survivors">this list</Link>
          </span>. Feel free from trade itens with non-infected humans.
        </p>
      </Column>
    </Columns>
  </div>
)

export default Home
