export default transactionItems => {
  if (!transactionItems || transactionItems.length < 1) {
    return false
  }

  const right = transactionItems.filter(x => x.from === 'right')
  const left = transactionItems.filter(x => x.from === 'left')

  const totalLeft = left.reduce((prev, next) => {
    return prev + next.amount * next.points
  }, 0)

  const totalRight = right.reduce((prev, next) => {
    return prev + next.amount * next.points
  }, 0)

  if (totalRight === totalLeft) {
    return true
  }

  return false
}
