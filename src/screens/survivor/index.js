import React from 'react'
import {Subscribe} from 'unstated'
import {lifecycle, compose, withHandlers, withProps} from 'recompose'
import Screen from './components/screen'
import PeopleContainer from './containers/people'

const enhance = compose(
  lifecycle({
    componentDidMount() {
      const {store, match} = this.props
      const {id} = match.params
      store.setState({loading: true})
      store.getPeople(id)
    },
    componentWillUnmount() {
      const {store} = this.props
      store.setState({loading: true})
    }
  }),
  withProps({
    headers: [
      {
        key: 'name',
        text: 'Description'
      },
      {
        key: 'quantity',
        text: 'Quantity'
      },
      {
        key: 'points',
        text: 'Points'
      }
    ]
  })
)

const Wrapper = enhance(Screen)

export default ({match}) => (
  <Subscribe to={[PeopleContainer]}>
    {store => <Wrapper store={store} match={match} />}
  </Subscribe>
)
