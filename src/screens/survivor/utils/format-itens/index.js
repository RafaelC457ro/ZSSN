export default invetory => {
  const itens = invetory.reduce((prev, {item, amount}) => {
    if (prev[item]) {
      prev[item] = Number(prev[item]) + Number(amount)
    } else {
      prev[item] = Number(amount)
    }
    return prev
  }, {})

  const final = Object.keys(itens)
    .map(key => {
      return `${key}:${itens[key]}`
    })
    .join(';')

  return final
}
