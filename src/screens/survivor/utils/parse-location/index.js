import isCoordinates from 'is-coordinates'

export default location => {
  if (typeof location !== 'string') {
    return null
  }
  if (location.match(/POINT[\s]+\([-|0-9\.\s]+\)/)) {
    const values = location
      .replace(/POINT[\s]+\(|POINT\(|\)/g, '')
      .split(/[\s]+/)
      .map(x => Number(x))

    if (!isCoordinates(values)) {
      return null
    }
    return {lat: values[0], lng: values[1]}
  }
  return null
}
