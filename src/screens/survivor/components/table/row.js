import React from 'react'

const Row = ({headers, item}) => (
  <tr>
    {headers
      .map(header => header.key)
      .map(key => <td key={key}> {item[key]}</td>)}
  </tr>
)

export default Row
