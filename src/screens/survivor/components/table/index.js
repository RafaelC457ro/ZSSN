import React from 'react'
import Row from './row'

const Table = ({headers, items}) => (
  <table className="table is-fullwidth is-bordered is-striped">
    <thead>
      <tr>{headers.map(header => <th key={header.key}>{header.text}</th>)}</tr>
    </thead>
    <tbody>
      {items.map(item => <Row key={item.name} headers={headers} item={item} />)}
    </tbody>
  </table>
)

export default Table
