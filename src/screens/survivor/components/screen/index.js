import React from 'react'
import {withFormik} from 'formik'
import Title from '../../../../components/ui/title'
import SubTitle from '../../../../components/ui/subtitle'
import Field from '../../../../components/ui/field'
import Columns from '../../../../components/ui/columns'
import Column from '../../../../components/ui/column'
import BreadCrumb from '../../../../components/ui/breadcrumb'
import Table from '../table'
import Map from '../map'
import parseLocation from '../../utils/parse-location'
import TradeModal from '../trade-modal'
import updateSurvivor from '../../services/update-person'
import formatLocation from '../../../../utils/format-location'
import ModalReport from '../report-modal'
import style from './style.css'

const Survivor = ({
  match,
  store,
  headers,
  errors,
  setFieldValue,
  handleSubmit,
  values
}) => {
  const {id} = match.params
  const {
    loading,
    people,
    showModalTrade,
    validTransaction,
    showModalReport
  } = store.state
  const {age, gender, name, lonlat, items} = people
  return (
    <div>
      <BreadCrumb
        paths={[
          {
            link: `survivor/${id}`,
            name: name ? String(name).toLowerCase() : 'unknow',
            isActive: true
          }
        ]}
      />
      {loading ? (
        <p>loading...</p>
      ) : (
        <form onSubmit={handleSubmit}>
          <Columns>
            <Column>
              <Title>{name}</Title>
              <SubTitle>
                {age} yeas old - <b>{gender == 'M' ? 'Male' : 'Female'}</b>
              </SubTitle>
              <div>
                <h2 className="subtitle">Inventory:</h2>
              </div>
              <Table headers={headers} items={items} />
              <div>
                <button
                  type="button"
                  className="button"
                  onClick={() => store.handleTrade()}
                >
                  trade items
                </button>
                {showModalTrade && (
                  <TradeModal
                    isActive={showModalTrade}
                    items={items}
                    trader={people}
                    handleClose={() => store.handleCloseTrade()}
                    handleTransactionChange={store.handleTransactionChange}
                  />
                )}

                <button
                  type="button"
                  className={`button is-danger ${style.space}`}
                  onClick={() => {
                    store.handleOpenMoldalReportAsInfected()
                  }}
                >
                  report as infected
                </button>
              </div>
            </Column>
            <Column>
              <Map location={parseLocation(lonlat)} onChange={setFieldValue} />
            </Column>
          </Columns>
          <hr />
          <button
            className="button is-success is-pulled-right "
            disabled={!values.location || Boolean(errors.location)}
          >
            Update Location
          </button>
        </form>
      )}
      {showModalReport && (
        <ModalReport
          isActive={showModalReport}
          reporterId={id}
          handleClose={() => store.handleCloseReportAsInfected()}
        />
      )}
    </div>
  )
}
const SurviveForm = withFormik({
  mapPropsToValues: () => ({
    location: null
  }),
  validate: values => {
    const errors = {}

    if (!values.location) {
      errors.location = 'not valid'
    }
    return errors
  },
  handleSubmit: (values, {props, setSubmitting, setErrors}) => {
    const {location} = values
    const {people} = props.store.state

    const payload = {
      id: people.id,
      survivor: {
        person: {
          name: people.name,
          age: Number(people.age),
          gender: people.gender,
          lonlat: formatLocation(location)
        }
      }
    }

    updateSurvivor(payload)
      .then(({id}) => {
        props.store.getPeople(id)
        setSubmitting(false)
      })
      .catch(err => {
        setSubmitting(false)
        alert('Error on update Location')
      })
  },
  displayName: 'Update Survivor'
})(Survivor)

export default SurviveForm
