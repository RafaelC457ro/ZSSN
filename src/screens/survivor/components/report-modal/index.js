import React from 'react'
import {withFormik} from 'formik'
import PropTypes from 'prop-types'
import classnames from 'classnames'
import SelectSurvivor from '../select-survivor'
import reportSurvivor from '../../services/report-people'
import style from './style.css'

const Modal = ({
  handleClose,
  isActive,
  values,
  handleSubmit,
  errors,
  setFieldValue
}) => {
  return (
    <div className={classnames('modal', {'is-active': isActive})}>
      <form onSubmit={handleSubmit}>
        <div className="modal-background" onClick={handleClose} />
        <div className={`modal-card ${style.card}`}>
          <header className="modal-card-head">
            <p className="modal-card-title">Report as Infected</p>
            <button
              className="delete"
              aria-label="close"
              onClick={handleClose}
            />
          </header>
          <section className="modal-card-body">
            <div className={style.text} onSubmit={handleSubmit}>
              Are you sure do you want to report
              <SelectSurvivor
                onChange={value => {
                  setFieldValue('suspectId', value.value)
                }}
              />
              as infected?
              <p>
                A survivor is marked as infected when at least three other
                survivors report their contamination. When a survivor is
                infected, their inventory items become inaccessible (they cannot
                trade with others).
              </p>
              {Boolean(errors.suspectId) && (
                <p className="has-text-danger">{errors.suspectId}</p>
              )}
            </div>
          </section>
          <footer className="modal-card-foot ">
            <button className="button is-success">Yes</button>
            <button
              className="button is-danger"
              type="button"
              onClick={handleClose}
            >
              No
            </button>
          </footer>
        </div>
      </form>
    </div>
  )
}

const SurviveForm = withFormik({
  mapPropsToValues: () => ({
    suspectId: null
  }),
  validate: values => {
    const errors = {}

    if (!values.suspectId) {
      errors.suspectId = 'Select a Person to report'
    }
    return errors
  },
  handleSubmit: (values, {props, setSubmitting, setErrors}) => {
    const {suspectId} = values

    reportSurvivor({
      id: props.reporterId,
      infected: suspectId
    })
      .then(({id}) => {
        setSubmitting(false)
        alert('Report goes ok')
        props.handleClose()
      })
      .catch(err => {
        setSubmitting(false)
        alert('Error on report Survivor')
      })
  },
  displayName: 'Update Survivor'
})(Modal)

export default SurviveForm
