import React from 'react'
import Select from 'react-select'
import {lifecycle, compose, withHandlers, withProps, withState} from 'recompose'
import 'react-select/dist/react-select.css'
import listPeoples from '../../../../services/peoples/list'
import getId from '../../../../utils/get-id'
import style from './style.css'

const SelectSurvivor = ({options, visualValue, handleChange}) => {
  return (
    <Select
      name="form-field-name"
      value={visualValue}
      searchable
      style={{
        backgroundColor: '#282F2F',
        color: '#eee !important',
        border: '2px solid #8C9B9D'
      }}
      onChange={handleChange}
      options={options}
    />
  )
}

const enhance = compose(
  withState('options', 'setOptions', []),
  withState('visualValue', 'setValue', ''),
  withHandlers({
    handleChange: props => value => {
      props.setValue(value)
      props.onChange && props.onChange(value)
    }
  }),
  lifecycle({
    componentDidMount() {
      listPeoples()
        .then(({peoples}) => {
          const options = peoples
            .filter(x => !x['infected?'])
            .map(({name, location}) => ({
              label: name,
              value: getId(location)
            }))

          this.setState({options})
        })
        .catch(err => {
          alert('Error on get peoples')
        })
    }
  })
)

export default enhance(SelectSurvivor)
