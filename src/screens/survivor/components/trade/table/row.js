import React from 'react'
import classname from 'classnames'

const Row = ({headers, item, selected, index, handleSelect, handleRemove}) => (
  <tr
    className={classname({
      'is-selected': selected
    })}
  >
    {headers.map(header => header.key).map(key => (
      <td
        key={key}
        onClick={() => {
          handleSelect && handleSelect(index)
        }}
      >
        {item[key]}
      </td>
    ))}
    {handleRemove && (
      <td>
        <button
          type="button"
          className="button is-danger is-small"
          onClick={() => handleRemove(index)}
        >
          remove
        </button>
      </td>
    )}
  </tr>
)

export default Row
