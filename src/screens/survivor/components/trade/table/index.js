import React from 'react'
import Row from './row'

const Table = ({headers, items, selected, handleSelect, handleRemove}) => (
  <table className="table is-fullwidth is-bordered is-striped">
    <thead>
      <tr>
        {headers.map(header => <th key={header.key}>{header.text}</th>)}
        {handleRemove && <th>Action</th>}
      </tr>
    </thead>
    <tbody>
      {items.map((item, index) => (
        <Row
          key={item.name || item.to + item.from + item.item + item.amount}
          headers={headers}
          item={item}
          index={index}
          selected={index === selected}
          handleSelect={handleSelect}
          handleRemove={handleRemove}
        />
      ))}
    </tbody>
  </table>
)

export default Table
