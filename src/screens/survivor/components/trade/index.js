import React from 'react'
import {compose, withHandlers, withProps, withState} from 'recompose'
import Columns from '../../../../components/ui/columns'
import Column from '../../../../components/ui/column'
import Map from '../map'
import Table from './table'
import ArrowRight from '../arrow-right'
import ArrowLeft from '../arrow-left'
import SelectSurvivor from '../select-survivor'
import getItensSurvivor from '../../services/get-people-properties'
import style from './style.css'

const Trade = ({
  leftItems,
  rightItems,
  itemsHeaders,
  transactionHeaders,
  leftSelected,
  rightSelected,
  handleSelectLeft,
  handleSelectRight,
  handleAmountChange,
  handleLeftTransaction,
  handleRightTransaction,
  amountItems,
  transationItems,
  handleRemoveTransaction,
  handleChoiceTrader,
  trader,
  validTransaction
}) => {
  return (
    <div>
      <Columns>
        <Column>
          <div className={style.traders}>
            <input type="text" className="input" value={trader.name} readOnly />
          </div>
          <Table
            headers={itemsHeaders}
            items={leftItems}
            selected={leftSelected}
            handleSelect={handleSelectLeft}
          />
        </Column>
        <Column size="2">
          <div className={style.control}>
            <div onClick={handleLeftTransaction}>
              <ArrowRight height="40px" width="40px" fill="#eee" />
            </div>
            <div>
              <input
                type="number"
                min="1"
                className="input"
                name="amount"
                value={amountItems}
                onChange={handleAmountChange}
              />
            </div>
            <div onClick={handleRightTransaction}>
              <ArrowLeft height="40px" width="40px" fill="#eee" />
            </div>
          </div>
        </Column>
        <Column>
          <div className={style.traders}>
            <SelectSurvivor onChange={handleChoiceTrader} />
          </div>
          <Table
            headers={itemsHeaders}
            items={rightItems}
            selected={rightSelected}
            handleSelect={handleSelectRight}
          />
        </Column>
      </Columns>
      <h2 className="title is-4">Transaction</h2>
      {!validTransaction ? (
        <span className="has-text-danger is-2">
          Both sides of the trade should offer the same amount of points
        </span>
      ) : null}
      <Table
        headers={transactionHeaders}
        items={transationItems}
        handleRemove={handleRemoveTransaction}
      />
    </div>
  )
}

const enhance = compose(
  withState('leftItems', 'setLeftItems', props => {
    return props.items
  }),
  withState('rightItems', 'setRightItems', []),
  withState('anotherTrader', 'setAnotherTrader', {name: ''}),
  withState('leftSelected', 'setLeftSelected', -1),
  withState('rightSelected', 'setRightSelected', -1),
  withState('amountItems', 'setAmountItems', 1),
  withState('transationItems', 'setTransationItems', []),
  withHandlers({
    handleSelectLeft: props => index => {
      props.setLeftSelected(index)
    },
    handleSelectRight: props => index => {
      props.setRightSelected(index)
    },
    handleAmountChange: props => change => {
      props.setAmountItems(change.target.value)
    },
    handleLeftTransaction: props => () => {
      const {
        leftSelected,
        rightItems,
        leftItems,
        amountItems,
        transationItems
      } = props
      if (
        leftSelected > -1 &&
        leftItems[leftSelected].quantity >= amountItems &&
        rightItems.length > 0
      ) {
        transationItems.push({
          from: 'left',
          to: 'right',
          fromName: props.trader.name,
          toName: props.anotherTrader.name,
          id: props.anotherTrader.id,
          item: leftItems[leftSelected].name,
          amount: amountItems,
          originalIndex: leftSelected,
          points: leftItems[leftSelected].points
        })

        leftItems[leftSelected].quantity =
          Number(leftItems[leftSelected].quantity) - Number(amountItems)

        props.setLeftItems(leftItems)
        props.setLeftSelected(-1)
        props.setTransationItems(transationItems)
        props.handleTransactionChange(transationItems)
      }
    },
    handleRightTransaction: props => () => {
      const {rightSelected, rightItems, amountItems, transationItems} = props
      if (
        rightSelected > -1 &&
        rightItems[rightSelected].quantity >= amountItems
      ) {
        transationItems.push({
          from: 'right',
          to: 'left',
          toName: props.trader.name,
          fromName: props.anotherTrader.name,
          item: rightItems[rightSelected].name,
          amount: amountItems,
          originalIndex: rightSelected,
          points: rightItems[rightSelected].points
        })

        rightItems[rightSelected].quantity =
          Number(rightItems[rightSelected].quantity) - Number(amountItems)

        props.setRightItems(rightItems)
        props.setRightSelected(-1)
        props.setTransationItems(transationItems)
        props.handleTransactionChange(transationItems)
      }
    },
    handleRemoveTransaction: props => index => {
      const {
        leftItems,
        rightItems,
        transationItems,
        setLeftItems,
        setRightItems
      } = props
      const subject = transationItems[index]

      if (subject.from === 'left') {
        leftItems[subject.originalIndex].quantity =
          leftItems[subject.originalIndex].quantity + Number(subject.amount)
        setLeftItems(leftItems)
      }

      if (subject.from === 'right') {
        rightItems[subject.originalIndex].quantity =
          Number(rightItems[subject.originalIndex].quantity) +
          Number(subject.amount)
        setRightItems(setRightItems)
      }
      transationItems.splice(index, 1)

      props.setTransationItems(transationItems)
      props.handleTransactionChange(transationItems)
    },
    handleChoiceTrader: props => ({value, label}) => {
      props.setAnotherTrader({
        id: value,
        name: label
      })
      getItensSurvivor(value)
        .then(items => {
          const rightItems = items.map(({item, quantity}) => {
            return {
              ...item,
              quantity
            }
          })
          props.setLeftItems(props.items)
          props.setRightItems(rightItems)
          props.setTransationItems([])
          props.handleTransactionChange([])
        })
        .catch(err => {
          alert(err)
        })
    }
  }),
  withProps({
    itemsHeaders: [
      {
        key: 'name',
        text: 'Description'
      },
      {
        key: 'quantity',
        text: 'Quantity'
      },
      {
        key: 'points',
        text: 'Points'
      }
    ],
    transactionHeaders: [
      {
        key: 'fromName',
        text: 'from'
      },
      {
        key: 'toName',
        text: 'to'
      },
      {
        key: 'item',
        text: 'Item'
      },
      {
        key: 'amount',
        text: 'Amount'
      }
    ]
  })
)

export default enhance(Trade)
