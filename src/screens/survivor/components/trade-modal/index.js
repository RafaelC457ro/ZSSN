import React from 'react'
import {compose, withHandlers, withProps, withState} from 'recompose'
import PropTypes from 'prop-types'
import classnames from 'classnames'
import validateTransaction from '../../validation/transaction'
import Trade from '../trade'
import formatItems from '../../utils/format-itens'
import tradeItems from '../../services/trade-itens'
import style from './style.css'

const Modal = ({
  isActive,
  items,
  handleClose,
  validTransaction,
  handleTransactionChange,
  handleTransaction,
  trader
}) => (
  <div className={classnames('modal', {'is-active': isActive})}>
    <div className="modal-background" onClick={handleClose} />
    <div className={`modal-card ${style.card}`}>
      <header className="modal-card-head">
        <p className="modal-card-title">Trade Items</p>
        <button className="delete" aria-label="close" onClick={handleClose} />
      </header>
      <section className="modal-card-body">
        <Trade
          items={items}
          trader={trader}
          validTransaction={validTransaction}
          handleTransactionChange={handleTransactionChange}
        />
      </section>
      <footer className="modal-card-foot ">
        <button
          className="button is-success"
          disabled={!validTransaction}
          onClick={handleTransaction}
        >
          Trade
        </button>
        <button className="button" onClick={handleClose}>
          Cancel
        </button>
      </footer>
    </div>
  </div>
)

const enhance = compose(
  withState('validTransaction', 'setValidTransaction', false),
  withState('transactionItens', 'setTransactionItens', []),
  withHandlers({
    handleTransactionChange: props => values => {
      const valid = validateTransaction(values)
      props.setValidTransaction(valid)
      props.setTransactionItens(values)
    },
    handleTransaction: props => () => {
      const {transactionItens, handleClose} = props

      const left = transactionItens.filter(x => x.to === 'left')
      const right = transactionItens.filter(x => x.to === 'right')

      const toId = left[0].id
      const toName = right[0].toName
      const fromId = props.trader.id

      const toItens = formatItems(left)
      const fromItens = formatItems(right)

      const payload = {
        id: fromId,
        trade: {
          consumer: {
            name: toName,
            pick: fromItens,
            payment: toItens
          }
        }
      }

      tradeItems(payload)
        .then(data => {
          console.log(data)
          handleClose()
        })
        .catch(err => {
          console.log(err)
        })
    }
  })
)

export default enhance(Modal)
