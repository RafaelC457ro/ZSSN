import React from 'react'
import {compose, withProps, withHandlers, withState, lifecycle} from 'recompose'
import {withScriptjs, withGoogleMap, GoogleMap, Marker} from 'react-google-maps'
import style from '../../../../theme/dark-map-style'

const Map = compose(
  lifecycle({
    componentDidMount() {
      const {location} = this.props
    }
  }),
  withProps({
    googleMapURL: `https://maps.googleapis.com/maps/api/js?v=3.exp&key=${
      process.env.GOOGLE_MAPS_KEY
    }&libraries=geometry,drawing,places`,
    loadingElement: <div style={{height: `100%`}} />,
    containerElement: <div style={{height: `400px`}} />,
    mapElement: <div style={{height: `100%`}} />
  }),
  withState('maker', 'setMaker', null),
  withHandlers({
    onMapClick: ({state, setMaker, showMaker, onChange}) => event => {
      const lat = event.latLng.lat()
      const lng = event.latLng.lng()
      setMaker({lat, lng})
      onChange && onChange('location', {lat, lng})
    }
  }),
  withScriptjs,
  withGoogleMap
)(({maker, location, onMapClick}) => (
  <GoogleMap
    defaultZoom={13}
    onClick={onMapClick}
    defaultCenter={location || {lat: -27.5949, lng: -48.5482}}
    defaultOptions={{styles: style}}
  >
    {(maker || location) && <Marker position={maker || location} />}
  </GoogleMap>
))

export default Map
