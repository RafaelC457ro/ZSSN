import qs from 'qs'
import axios from 'axios'

export default ({id, trade}) => {
  const options = {
    method: 'POST',
    headers: {'content-type': 'application/x-www-form-urlencoded'},
    data: qs.stringify(trade),
    url: process.env.BASE_URL + `/api/people/${id}/properties/trade_item.json`
  }

  return axios(options)
    .then(response => response.data)
    .catch(err => {
      const hasResponse = Boolean(err.response)

      if (hasResponse) {
        const error = err.response.data

        if (err.response.status === 422) {
          throw new Error(error)
        }
      }
      throw err
    })
}
