import axios from 'axios'

export default function (id) {
  return axios
    .get(process.env.BASE_URL + `/api/people/${id}`)
    .then(response => response.data)
    .then(payload => {
      return payload
    })
}
