import {Container} from 'unstated'
import getPeople from '../../services/get-people'
import getPeopleProperties from '../../services/get-people-properties'

class People extends Container {
  // eslint-disable-next-line no-undef
  state = {
    people: {},
    loading: true,
    showModalTrade: false,
    validTransaction: false,
    transactionItens: [],
    isLocationChange: false,
    newLocation: null,
    showModalReport: false
  }

  getPeople(id) {
    this.setState({loading: true})
    Promise.all([getPeople(id), getPeopleProperties(id)])
      .then(([people, properties]) => {
        const items = properties.map(({item, quantity}) => {
          return {
            ...item,
            quantity
          }
        })
        this.setState({
          loading: false,
          people: {...people, items}
        })
      })
      .catch(err => {
        this.setState({
          loading: false
        })
        alert('Error on get People')
      })
  }

  handleTrade() {
    this.setState({showModalTrade: true})
  }

  handleCloseTrade() {
    this.getPeople(this.state.people.id)
    this.setState({showModalTrade: false})
  }

  handleCloseReportAsInfected() {
    this.setState({showModalReport: false})
  }

  handleOpenMoldalReportAsInfected() {
    this.setState({showModalReport: true})
  }

  changeLocation() {
    console.log(formatLocation(this.state.newLocation))
  }
}

export default People
