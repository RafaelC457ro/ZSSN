function InvalidFieldsException(fields) {
  this.fields = fields
  this.name = 'InvalidFieldsException'
}

InvalidFieldsException.prototype = new Error()
InvalidFieldsException.prototype.name = InvalidFieldsException.name
InvalidFieldsException.prototype.constructor = InvalidFieldsException

export default InvalidFieldsException
