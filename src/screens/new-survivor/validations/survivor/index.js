export default ({name, age, invetory, gender, location}) => {
  const errors = {}
  if (name.trim() == '') {
    errors.name = 'Name is required'
  }

  if (gender.trim() == '') {
    errors.gender = 'Gender is required'
  }

  if (Number(age) < 1) {
    errors.age = 'Age is required'
  }
  if (invetory.length < 1) {
    errors.invetory = 'Itens in inventory is required'
  }

  if (!location.lat && !location.lat) {
    errors.location = 'Location is required'
  }
  return errors
}
