import React from 'react'
import classname from 'classnames'
import Title from '../../../../components/ui/title'
import SubTitle from '../../../../components/ui/subtitle'
import Field from '../../../../components/ui/field'
import Select from '../../../../components/ui/select'
import Columns from '../../../../components/ui/columns'
import Column from '../../../../components/ui/column'
import Inventory from '../inventory'
import Map from '../map'

const Form = ({
  values,
  errors,
  handleSubmit,
  handleChange,
  handleBlur,
  setFieldValue,
  setFieldTouched,
  isSubmitting
}) => (
  <form onSubmit={handleSubmit}>
    <Columns>
      <Column>
        <Field
          label="Name"
          type="text"
          id="input-name"
          name="name"
          values={values.name}
          isDanger={Boolean(errors.name)}
          errorMessage={errors.name}
          onChange={handleChange}
          onBlur={handleBlur}
          placeholder="Type name"
        />
        <Columns>
          <Column>
            <Field
              label="Age"
              type="number"
              min="1"
              step="1"
              id="input-age"
              name="age"
              values={values.name}
              isDanger={Boolean(errors.age)}
              errorMessage={errors.age}
              onChange={handleChange}
              onBlur={handleBlur}
              placeholder="Type age"
            />
          </Column>
          <Column>
            <Select
              id="input-gender"
              name="gender"
              label="Gender"
              values={values.gender}
              isDanger={Boolean(errors.gender)}
              errorMessage={errors.gender}
              onChange={handleChange}
              onBlur={handleBlur}
              options={[
                {text: 'Select a gender', value: ''},
                {text: 'Male', value: 'M'},
                {text: 'Female', value: 'F'}
              ]}
            />
          </Column>
        </Columns>
        <hr />
        <h2 className="title is-4">Inventory</h2>
        <Inventory onChange={setFieldValue} errorMessage={errors.invetory} />
      </Column>
      <Column>
        <h2 className="title is-4">Location</h2>
        <SubTitle>
          lat: {values.location.lat} lng: {values.location.lat}
        </SubTitle>
        <Map onChange={setFieldValue} />
        {errors.location && (
          <span className="has-text-danger">{errors.location}</span>
        )}
      </Column>
    </Columns>
    <hr />
    <div className="is-pulled-right">
      <button
        className={classname('button is-success', {
          'is-loading': isSubmitting
        })}
      >
        Register
      </button>
    </div>
  </form>
)

export default Form
