import React from 'react'
import {compose, withProps, withHandlers, withState} from 'recompose'
import {withScriptjs, withGoogleMap, GoogleMap, Marker} from 'react-google-maps'
import style from '../../../../theme/dark-map-style'

const Map = compose(
  withProps({
    googleMapURL: `https://maps.googleapis.com/maps/api/js?v=3.exp&key=${
      process.env.GOOGLE_MAPS_KEY
    }&libraries=geometry,drawing,places`,
    loadingElement: <div style={{height: `100%`}} />,
    containerElement: <div style={{height: `400px`}} />,
    mapElement: <div style={{height: `100%`}} />
  }),
  withState('maker', 'setMaker', {lat: -27.5949, lng: -48.5482}),
  withState('isMarkerShown', 'showMaker', false),
  withHandlers({
    onMapClick: ({state, setMaker, showMaker, onChange}) => event => {
      const lat = event.latLng.lat()
      const lng = event.latLng.lng()
      setMaker({lat, lng})
      showMaker(true)
      onChange('location', {lat, lng})
    }
  }),
  withScriptjs,
  withGoogleMap
)(({isMarkerShown, maker, onMapClick}) => (
  <GoogleMap
    defaultZoom={13}
    onClick={onMapClick}
    defaultCenter={{lat: -27.5949, lng: -48.5482}}
    defaultOptions={{styles: style}}
  >
    {isMarkerShown && <Marker position={maker} />}
  </GoogleMap>
))

export default Map
