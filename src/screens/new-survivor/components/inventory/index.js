import React, {Component} from 'react'
import Field from '../../../../components/ui/field'
import Select from '../../../../components/ui/select'
import Columns from '../../../../components/ui/columns'
import Column from '../../../../components/ui/column'

import Table from './table'

class Invetory extends Component {
  constructor(props) {
    super(props)
    this.headers = [
      {
        key: 'quantity',
        text: 'Quantity'
      },
      {
        key: 'type',
        text: 'Description'
      },
      {
        key: 'action',
        text: 'Action'
      }
    ]
    this.options = [
      {
        text: 'Water',
        value: 'Water'
      },
      {
        text: 'Food',
        value: 'Food'
      },
      {
        text: 'Medication',
        value: 'Medication'
      },
      {
        text: 'Ammunition',
        value: 'Ammunition'
      }
    ]
    this.state = {
      quantity: 1,
      type: 'Water',
      items: []
    }
    this.handleChange = this.handleChange.bind(this)
    this.addItem = this.addItem.bind(this)
    this.handleRemove = this.handleRemove.bind(this)
  }
  handleChange(event) {
    const change = {}
    change[event.target.name] = event.target.value
    this.setState(change)
  }

  addItem() {
    const {quantity, type, items} = this.state
    items.push({quantity, type})
    this.setState({items, quantity: 1, type: 'Water'})
    this.props.onChange('invetory', items)
  }

  handleRemove(item) {
    const index = this.state.items.indexOf(item)
    const items = this.state.items.splice(index, 1)
    this.setState({item})
    this.props.onChange('invetory', items)
  }

  render() {
    const {items, quantity, type} = this.state
    return (
      <div>
        <Columns>
          <Column>
            <Field
              label={false}
              type="number"
              name="quantity"
              value={quantity}
              step="1"
              min="1"
              className="input is-fullwidth"
              placeholder="Type Quantity of Items"
              onChange={this.handleChange}
            />
          </Column>
          <Column>
            <Select
              className="input is-fullwidth"
              label={false}
              options={this.options}
              value={type}
              name="type"
              onChange={this.handleChange}
            />
          </Column>
          <Column>
            <button
              type="button"
              className="button is-success is-fullwidth"
              onClick={this.addItem}
            >
              Add
            </button>
          </Column>
        </Columns>
        <Table
          headers={this.headers}
          items={items}
          handleRemove={this.handleRemove}
        />
        {this.props.errorMessage && (
          <span className="has-text-danger">{this.props.errorMessage}</span>
        )}
      </div>
    )
  }
}

export default Invetory
