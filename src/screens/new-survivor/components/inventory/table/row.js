import React from 'react'
import style from './style.css'

const Row = ({headers, item, handleRemove}) => (
  <tr>
    {headers
      .map(header => header.key)
      .filter(header => header !== 'action')
      .map(key => <td key={key}> {item[key]}</td>)}
    <td>
      <div className={style.action}>
        <button
          type="button"
          className="button is-small is-danger"
          onClick={() => handleRemove(item)}
        >
          remove
        </button>
      </div>
    </td>
  </tr>
)

export default Row
