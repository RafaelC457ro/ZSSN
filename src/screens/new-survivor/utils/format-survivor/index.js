import formatInvetory from '../format-invetory'
import formatLocation from '../../../../utils/format-location'

export default ({name, age, gender, invetory, location}) => {
  return {
    person: {
      name,
      age: Number(age),
      gender,
      lonlat: formatLocation(location)
    },
    items: formatInvetory(invetory)
  }
}
