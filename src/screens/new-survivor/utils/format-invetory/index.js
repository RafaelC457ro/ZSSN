export default invetory => {
  const itens = invetory.reduce((prev, {type, quantity}) => {
    if (prev[type]) {
      prev[type] = Number(prev[type]) + Number(quantity)
    } else {
      prev[type] = Number(quantity)
    }
    return prev
  }, {})

  const final = Object.keys(itens)
    .map(key => {
      return `${key}:${itens[key]}`
    })
    .join(';')

  return final
}
