export default ({name, gender, age, items, lonlat}) => {
  return {
    name: name && name[0],
    gender: gender && gender[0],
    age: age && age[0],
    location: lonlat && lonlat[0]
  }
}
