import qs from 'qs'
import axios from 'axios'
import InvalidFieldsException from '../../exceptions/invalid-fields-exception'

export default survivor => {
  const options = {
    method: 'POST',
    headers: {'content-type': 'application/x-www-form-urlencoded'},
    data: qs.stringify(survivor),
    url: process.env.BASE_URL + `/api/people.json`
  }

  return axios(options)
    .then(response => response.data)
    .catch(err => {
      const hasResponse = Boolean(err.response)

      if (hasResponse) {
        const error = err.response.data

        if (err.response.status === 422) {
          throw new InvalidFieldsException(error)
        }
      }
      throw err
    })
}
