import React from 'react'
import {withFormik} from 'formik'
import BreadCrumb from '../../components/ui/breadcrumb'
import Title from '../../components/ui/title'
import Form from './components/form'
import survivorValidation from './validations/survivor'
import createSurvivor from './services/people/create'
import formatSurvivor from './utils/format-survivor'
import InvalidFieldsException from './exceptions/invalid-fields-exception'
import formatErrors from './utils/format-errors'

const EnhancedForm = withFormik({
  mapPropsToValues: () => ({
    name: '',
    age: '',
    gender: '',
    invetory: [],
    location: {}
  }),
  validate: survivorValidation,
  handleSubmit: (values, {props, setSubmitting, setErrors}) => {
    const survivor = formatSurvivor(values)

    createSurvivor(survivor)
      .then(({id}) => {
        setSubmitting(false)
        props.history.push(`/survivor/${id}`)
      })
      .catch(err => {
        setSubmitting(false)

        if (err instanceof InvalidFieldsException) {
          setErrors(formatErrors(err.fields))
        }
      })
  },
  displayName: 'New Survivor'
})(Form)

const Survivor = props => (
  <div>
    <BreadCrumb
      paths={[{link: 'survivor/new', name: 'new survivor', isActive: true}]}
    />
    <Title>New Survivor</Title>
    <EnhancedForm {...props} />
  </div>
)

export default Survivor
