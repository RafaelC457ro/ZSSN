import axios from 'axios'

export default function () {
  return axios
    .get(process.env.BASE_URL + '/api/report/infected_points')
    .then(response => response.data)
    .then(payload => {
      return payload
    })
}
