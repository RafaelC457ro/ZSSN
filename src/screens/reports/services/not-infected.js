import axios from 'axios'

export default function () {
  return axios
    .get(process.env.BASE_URL + '/api/report/non_infected')
    .then(response => response.data)
    .then(payload => {
      return payload
    })
}
