import React from 'react'
import {lifecycle, compose, withHandlers, withProps, withState} from 'recompose'
import Title from '../../components/ui/title'
import BreadCrumb from '../../components/ui/breadcrumb'
import getInfected from './services/infected'
import getNonInfected from './services/not-infected'
import getPeopleInvetory from './services/people-inventory'
import getInfectedPoints from './services/infected-points'

const Reports = ({isLoading, reports}) => (
  <div>
    <BreadCrumb paths={[{link: 'reports', name: 'reports', isActive: true}]} />
    <Title>Reports</Title>
    {isLoading ? (
      <p>loading...</p>
    ) : (
      <div>
        {reports.map(report => {
          return (
            <div key={report.description}>
              <h2 className="title has-text-success">{report.description}</h2>
              {report.items.map(item => {
                return <p key={item[0]}>{`${item[0]} : ${item[1]}`}</p>
              })}
            </div>
          )
        })}
      </div>
    )}
  </div>
)

const enhance = compose(
  withState('isLoading', 'setLoading', true),
  withState('reports', 'setReports', []),
  lifecycle({
    componentDidMount() {
      Promise.all([
        getInfected(),
        getNonInfected(),
        getPeopleInvetory(),
        getInfectedPoints()
      ])
        .then(data => {
          const reports = data.map(({report}) => {
            const description = report.description
            const items = Object.keys(report)
              .filter(x => x !== 'description')
              .map(key => {
                return [key.split('_').join(' '), report[key]]
              })
            return {
              description,
              items
            }
          })

          this.setState({
            isLoading: false,
            reports
          })
        })
        .catch(err => {
          this.setState({
            isLoading: false
          })
          console.log(err)
        })
    }
  })
)

export default enhance(Reports)
