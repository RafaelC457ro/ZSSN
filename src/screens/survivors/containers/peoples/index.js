import {Container} from 'unstated'
import listPeoples from '../../../../services/peoples/list'

class Peoples extends Container {
  // eslint-disable-next-line no-undef
  state = {
    peoples: [],
    loading: true
  }

  getPeoples() {
    this.setState({loading: true})

    listPeoples()
      .then(({peoples}) => {
        this.setState({
          loading: false,
          peoples
        })
      })
      .catch(err => {
        this.setState({
          loading: false
        })
        alert('Error on list Survivors')
      })
  }

  searchPeople(search) {
    this.setState({loading: true})
    listPeoples()
      .then(({peoples}) => {
        const filtredPeoples = peoples.filter(people => {
          const name = people.name.toLowerCase()
          return name.includes(search.toLowerCase())
        })

        this.setState({
          peoples: filtredPeoples,
          loading: false
        })
      })
      .catch(err => {
        this.setState({
          loading: false
        })
        alert('Error on Search Survivors')
      })
  }
}

export default Peoples
