import React from 'react'
import {Subscribe} from 'unstated'
import {lifecycle, compose, withHandlers, withProps} from 'recompose'
import Screen from './components/screen'
import PeoplesContainer from './containers/peoples'

const enhance = compose(
  lifecycle({
    componentDidMount() {
      const {store} = this.props
      store.setState({loading: true})
      store.getPeoples()
    },
    componentWillUnmount() {
      const {store} = this.props
      store.setState({loading: true})
    }
  }),
  withHandlers({
    handleSearch: ({store}) => search => {
      if (search.trim() === '') {
        store.getPeoples()
      } else {
        store.searchPeople(search)
      }
    }
  }),
  withProps({
    headers: [
      {
        key: 'name',
        text: 'Nome'
      },
      {
        key: 'age',
        text: 'Age'
      },
      {
        key: 'gender',
        text: 'Gender'
      },
      {
        key: 'infected?',
        text: 'Infected'
      },
      {
        key: 'action',
        text: 'Action'
      }
    ]
  })
)

const Wrapper = enhance(Screen)

export default ({}) => (
  <Subscribe to={[PeoplesContainer]}>
    {store => <Wrapper store={store} />}
  </Subscribe>
)
