import React from 'react'
import {withFormik} from 'formik'

const Search = ({
  values,
  handleSubmit,
  handleChange,
  handleBlur,
  isSubmitting
}) => (
  <form onSubmit={handleSubmit}>
    <div className="field has-addons">
      <p className="control">
        <input
          id="search"
          className="input"
          name="search"
          type="text"
          values={values.name}
          onChange={handleChange}
          onBlur={handleBlur}
          placeholder="search by name.."
        />
      </p>
      <p className="control">
        <button type="submit" className="button" disabled={isSubmitting}>
          Search
        </button>
      </p>
    </div>
  </form>
)

const EnhancedForm = withFormik({
  mapPropsToValues: () => ({search: ''}),
  handleSubmit: (values, {props, setSubmitting}) => {
    const {handleSearch} = props
    const {search} = values

    handleSearch(search)
    setSubmitting(false)
  },
  displayName: 'Search Componentn'
})(Search)

export default EnhancedForm
