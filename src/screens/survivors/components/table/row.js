import React from 'react'
import PropTypes from 'prop-types'
import classnames from 'classnames'
import {Link} from 'react-router-dom'
import getId from '../../../../utils/get-id'
import style from './style.css'

const isInfected = flag => (flag ? 'Yes' : 'No')

const Row = ({headers, item}) => (
  <tr>
    {headers
      .map(header => header.key)
      .filter(header => header !== 'action')
      .map(key => (
        <td
          key={key}
          className={classnames({'has-text-danger': item['infected?']})}
        >
          {key === 'infected?' ? isInfected(item[key]) : item[key]}
        </td>
      ))}
    <td>
      <div className={style.action}>
        {item['infected?'] ? (
          <button
            type="button"
            className="button is-primary is-small"
            disabled="true"
          >
            view
          </button>
        ) : (
          <Link
            to={`/survivor/${getId(item.location)}`}
            className="button is-primary is-small"
          >
            view
          </Link>
        )}
      </div>
    </td>
  </tr>
)

Row.propTypes = {
  headers: PropTypes.array.isRequired,
  item: PropTypes.object.isRequired
}

export default Row
