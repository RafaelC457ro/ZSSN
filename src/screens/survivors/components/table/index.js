import React from 'react'
import PropTypes from 'prop-types'

import Row from './row'

const Table = ({headers, items}) => (
  <div className="table-container">
    <table className="table is-fullwidth is-bordered is-striped">
      <thead>
        <tr>
          {headers.map(header => <th key={header.key}>{header.text}</th>)}
        </tr>
      </thead>
      <tbody>
        {items.map(item => (
          <Row key={item.name} headers={headers} item={item} />
        ))}
      </tbody>
    </table>
  </div>
)

Table.propTypes = {
  headers: PropTypes.array.isRequired,
  items: PropTypes.array.isRequired
}

export default Table
