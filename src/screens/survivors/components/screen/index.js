import React from 'react'
import PropTypes from 'prop-types'
import Title from '../../../../components/ui/title'
import Columns from '../../../../components/ui/columns'
import Column from '../../../../components/ui/column'
import BreadCrumb from '../../../../components/ui/breadcrumb'
import Search from '../search'
import Table from '../table'

const Survivors = ({store, headers, handleSearch}) => {
  const {peoples, loading} = store.state
  return (
    <div>
      <BreadCrumb
        paths={[{link: 'survivors', name: 'survivors', isActive: true}]}
      />
      <Title>Survivors</Title>
      <div>
        <Columns>
          <Column size="1/4">
            <Search handleSearch={handleSearch} />
          </Column>
        </Columns>
        {loading ? (
          <p>loading...</p>
        ) : (
          <Table headers={headers} items={peoples} />
        )}
      </div>
    </div>
  )
}

Survivors.propTypes = {
  headers: PropTypes.array.isRequired,
  store: PropTypes.object.isRequired
}

export default Survivors
