import axios from 'axios'

export default function () {
  return axios
    .get(process.env.BASE_URL + '/api/people.json')
    .then(response => response.data)
    .then(payload => {
      return {peoples: payload}
    })
}
