import React from 'react'
import PropTypes from 'prop-types'
import classnames from 'classnames'

const BreadCrumb = ({paths}) => (
  <nav className="breadcrumb" aria-label="breadcrumbs">
    <ul>
      <li>
        <a href="/">home</a>
      </li>
      {paths.map(({link, name, isActive}) => (
        <li key={link} className={classnames({'is-active': isActive})}>
          <a href={link}>{name}</a>
        </li>
      ))}
    </ul>
  </nav>
)

BreadCrumb.propTypes = {
  paths: PropTypes.array.isRequired
}
export default BreadCrumb
