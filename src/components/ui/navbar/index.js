import React from 'react'
import PropTypes from 'prop-types'
import NavbarItem from '../navbar-item'
import Container from '../container'
import Logo from '../logo'

const Navbar = ({links}) => (
  <nav className="navbar is-dark">
    <Container>
      <div className="navbar-start">
        <Logo />
      </div>
      <ul className="navbar-end">
        {links.map(({href, text}) => (
          <NavbarItem key={href} href={href} text={text} />
        ))}
      </ul>
    </Container>
  </nav>
)

Navbar.propTypes = {
  links: PropTypes.arrayOf(
    PropTypes.shape({
      href: PropTypes.string.isRequired,
      text: PropTypes.string.isRequired
    })
  ).isRequired
}

export default Navbar
