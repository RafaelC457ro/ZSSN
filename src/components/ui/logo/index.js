import React from 'react'
import {Link} from 'react-router-dom'
import style from './style.css'

const Logo = () => (
  <li className="navbar-item">
    <Link className={style.logo} to="/">
      ZSSN
    </Link>
  </li>
)

export default Logo
