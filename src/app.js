import React from 'react'
import Async from 'react-code-splitting'
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom'
import {Provider} from 'unstated'
import BaseScreen from './containers/base-screen'
import './app.scss'

const Home = props => (
  <Async load={import('./screens/home')} componentProps={props} />
)

const Survivors = props => (
  <Async load={import('./screens/survivors')} componentProps={props} />
)

const NewSurvivor = props => (
  <Async load={import('./screens/new-survivor')} componentProps={props} />
)

const Survivor = props => (
  <Async load={import('./screens/survivor')} componentProps={props} />
)

const Reports = props => (
  <Async load={import('./screens/reports')} componentProps={props} />
)

const NoMatch = props => (
  <Async load={import('./screens/no-match')} componentProps={props} />
)

const App = () => (
  <Provider>
    <Router>
      <Switch>
        <Route>
          <BaseScreen>
            <Switch>
              <Route exact path="/" component={Home} />
              <Route exact path="/survivors" component={Survivors} />z
              <Route exact path="/survivor/new" component={NewSurvivor} />
              <Route exact path="/survivor/:id" component={Survivor} />
              <Route exact path="/reports" component={Reports} />
              <Route component={NoMatch} />
            </Switch>
          </BaseScreen>
        </Route>
      </Switch>
    </Router>
  </Provider>
)

export default App
