export default function range(start, end) {
  const acc = []

  for (let i = start; end - i > 0; i++) {
    acc.push(i)
  }

  return acc
}
