export default url => {
  return url.split('/').reverse()[0]
}
