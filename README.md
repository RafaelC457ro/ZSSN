# ZSSN

Zombie Survival Social Network

```sh
# Install the dependencies
$ npm cp .env.exemple .env
$ npm i

# Start a webpack build in watch mode and a webserver with livereloading
$ npm start
```

Now access the applicattion at [localhost:3000](http://localhost:3000) in your 
machine, or you can see here [https://zssn.netlify.com/](https://zssn.netlify.com/)
